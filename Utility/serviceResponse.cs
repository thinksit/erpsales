﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Utility
{
    public class serviceResponse
    {
        public HttpStatusCode Status { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
