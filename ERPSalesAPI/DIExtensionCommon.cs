﻿using DB.Options.Implementation;
using DB.Options.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPSalesAPI
{
    public static class DIExtensionCommon
    {
        public static void ConfigureDICommon(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IDbContext, DbContext>();
        }
    }
}
