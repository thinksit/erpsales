﻿using Admin.DbModels.Parameters.Dashboard;
using Admin.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPSalesAPI.Areas.Admin.Controllers
{
    [Route("Admin/[controller]")]
    [ApiController]
    public class DashboardController : BaseController
    {
        private readonly IDashboardApiService _DashboardApiService;
        public DashboardController(IDashboardApiService DashboardApiService) 
        {
            _DashboardApiService = DashboardApiService;

            
        }
        [HttpPost]
        [Route("DemoInsert")]
        public async Task<IActionResult> DemoInsert([FromBody] DemoInsertRequest obj)
        {
            var res = await _DashboardApiService.DemoInsert(obj);
            return Ok(res);

        }
        [HttpPost]
        [Route("GetListData")]
        public async Task<IActionResult> GetListData()
        {
            var res = await _DashboardApiService.GetListData();
            return Ok(res);

        }
    }
}
