﻿using Admin.Repository.Implementation;
using Admin.Repository.Interface;
using Admin.Services.Implementation;
using Admin.Services.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERPSalesAPI
{
    public static class DIExtensionAdmin
    {
        public static void ConfigureDIAdmin(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IDashboardApiService, DashboardApiService>();
            services.AddTransient<IDashboardApiRepo, DashboardApiRepo>();
        }
    }
}
