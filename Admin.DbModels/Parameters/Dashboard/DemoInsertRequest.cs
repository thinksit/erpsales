﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Admin.DbModels.Parameters.Dashboard
{
    public class DemoInsertRequest
    {
        public string appName { get; set; }
        public string appPassword { get; set; }
    }
}
