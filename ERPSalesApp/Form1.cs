﻿using ERPSalesApp.Model.Common;
using ERPSalesApp.Model.Request;
using ERPSalesApp.Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERPSalesApp
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clsCommon objclsCommon = new clsCommon();
            DemoInsertRequest obj = new DemoInsertRequest();
            obj.appName = textBox1.Text.ToString();
            obj.appPassword = textBox2.Text.ToString();
            var vResponse = objclsCommon.IM_InvokePostMethod(obj, "Admin/Dashboard/DemoInsert");
            string str = "";
            GetDataList();
        }



        private void button2_Click(object sender, EventArgs e)
        {
            GetDataList();
        }
        public void GetDataList()
        {
            try
            {
                dataGridView1.AutoGenerateColumns = false; // dgvEmp is DataGridView name  
                clsCommon objclsCommon = new clsCommon();
                var vResponse = objclsCommon.IM_InvokePostMethod(null, "Admin/Dashboard/GetListData");
                GetDataList myDeserializedClass = JsonConvert.DeserializeObject<GetDataList>(vResponse);
                DataTable dataTable = objclsCommon.ConvertToDataTable(myDeserializedClass.result);
                dataGridView1.DataSource = dataTable;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetDataList();
        }
    }
}
