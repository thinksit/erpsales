﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERPSalesApp.Model.Response
{
    public class GetDataList
    {
        public int status { get; set; }
        public bool success { get; set; }
        public object message { get; set; }
        public List<Result> result { get; set; }
    }
    public class Result
    {
        public int appDemoId { get; set; }
        public string appUserName { get; set; }
        public string appPassword { get; set; }
    }
}
