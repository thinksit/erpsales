﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERPSalesApp.Model.Request
{
    public class DemoInsertRequest
    {
        public string appName { get; set; }
        public string appPassword { get; set; }
    }
}
