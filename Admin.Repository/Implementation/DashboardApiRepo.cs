﻿using Admin.DbModels.Parameters.Dashboard;
using Admin.Repository.Interface;
using Dapper;
using DB.Options.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Admin.Repository.Implementation
{
    public class DashboardApiRepo : IDashboardApiRepo
    {
        private readonly IDbContext _DbContext;

        public DashboardApiRepo(IDbContext DbContext)
        {
            _DbContext = DbContext;
        }
        public async Task<(int, bool)> DemoInsert(DemoInsertRequest obj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@appUserName", obj.appName);
                param.Add("@appPassword", obj.appPassword);
                var Response = await _DbContext.ExecutableData(clsSP.Sp_Dashboard_DemoInsert, param, CommandType.StoredProcedure);
                if (Response > 0)
                {
                    return (1, true);
                }
                else
                {
                    return (0, false);
                }
            }
            catch (Exception ex)
            {
                return (0, false);
            }
        }

        public async Task<IEnumerable<dynamic>> GetListData()
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                var Response = await _DbContext.GetDataList<dynamic>(clsSP.Sp_Dashboard_GetData, null, CommandType.StoredProcedure);
                return Response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
