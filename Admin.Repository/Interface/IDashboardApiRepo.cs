﻿using Admin.DbModels.Parameters.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Admin.Repository.Interface
{
    public interface IDashboardApiRepo
    {
        Task<(int,bool)> DemoInsert(DemoInsertRequest obj);
        Task<IEnumerable<dynamic>> GetListData();
    }
}
