﻿using Admin.DbModels.Parameters.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Admin.Services.Interface
{
    public interface IDashboardApiService
    {
        Task<serviceResponse> DemoInsert(DemoInsertRequest obj);
        Task<serviceResponse> GetListData();
    }
}
