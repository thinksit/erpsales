﻿using Admin.DbModels.Parameters.Dashboard;
using Admin.Repository.Interface;
using Admin.Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Admin.Services.Implementation
{
    public class DashboardApiService : IDashboardApiService
    {
        private readonly IDashboardApiRepo _IDashboardApiRepo;

        public DashboardApiService(IDashboardApiRepo IDashboardApiRepo)
        {
            _IDashboardApiRepo = IDashboardApiRepo;
        }

        public async Task<serviceResponse> DemoInsert(DemoInsertRequest obj)
        {
            serviceResponse response = new serviceResponse();
            try
            {
                var Result = await _IDashboardApiRepo.DemoInsert(obj);
                if (Result.Item1 > 0)
                {
                    response.Success = true;
                    response.Message = "Added successfully";
                }
                else
                {
                    response.Success = false;
                    response.Message = "Added Failed.!";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        public async Task<serviceResponse> GetListData()
        {
            serviceResponse response = new serviceResponse();
            try
            {
                var Result = await _IDashboardApiRepo.GetListData();
                response.Result = Result;
                response.Success = true;

            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }
    }
}
